```
#!groovy
This is a repository of a SoapUI test project for public ArticleService. 
Full service information can be found here: http://www.service-repository.com/service/overview/1875112799

initial version was created with the SoapUI Pro snapshot 5.0.0, however any other client can be used for test development. 
All the information about the tool can be fould here: http://www.soapui.org
Preferable scripting language: Groovy (but JavaScript can be used also)


NOTE:
You have to create separate branch with Your name and work there. You are not allowed to commit to master branch.

```